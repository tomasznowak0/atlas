import React from "react";
import { View, Text } from "react-native";

const WorkingAlert = ({ working }) => {
  if (!working) return null;
  return (
    <View
      style={{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        marginHorizontal: "auto",
        backgroundColor: "#ffffff90",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Text style={{ fontWeight: "900", fontSize: 80 }}>...</Text>
    </View>
  );
};

export default WorkingAlert;
