"use strict";
import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ToastAndroid,
} from "react-native";
import COLORS from "../constants/Colors";
import { Icon } from "expo";
import WorkingAlert from "../components/WorkingAlert";

const shadowStyles = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,

  elevation: 5,
};

const connect = (pin, status) => {
  fetch(`http://192.168.1.1/pin${pin}${status}`);
};

const DotLine = () => (
  <View
    style={{
      height: 50,
      width: 2,
      backgroundColor: COLORS.primary,
      alignItems: "space-around",
      justifyContent: "space-around",
    }}
  >
    <View style={{ height: 5, width: 2, backgroundColor: "#fff" }} />
    <View style={{ height: 5, width: 2, backgroundColor: "#fff" }} />
    <View style={{ height: 5, width: 2, backgroundColor: "#fff" }} />
    <View style={{ height: 5, width: 2, backgroundColor: "#fff" }} />
  </View>
);
const RoundBtn = ({ icon, text, ...rest }) => (
  <TouchableOpacity
    {...rest}
    style={{
      backgroundColor: COLORS.primary + "90",
      borderRadius: 70,
      width: 80,
      height: 80,
      alignItems: "center",
      justifyContent: "center",
      // ...shadowStyles,
    }}
  >
    <Text style={{ fontSize: 25, fontWeight: "900", color: "#fff" }}>{text}</Text>
  </TouchableOpacity>
);

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  state = { working: false };

  onLong = pin => {
    this.setState({ working: true });
    connect(
      pin,
      "off"
    );
  };

  onPressOut = pin => {
    this.setState({ working: false });
    connect(
      pin,
      "on"
    );
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <View style={{ alignItems: "center", justifyContent: "center" }}>
            <RoundBtn
              activeOpacity={0.8}
              icon="md-wine"
              text={1}
              onLongPress={() => this.onLong(25)}
              onPressOut={() => this.onPressOut(25)}
            />
            <DotLine />
            <RoundBtn
              activeOpacity={0.8}
              icon="md-wine"
              text={2}
              onLongPress={() => this.onLong(26)}
              onPressOut={() => this.onPressOut(26)}
            />
          </View>
          {/* <View style={{ alignItems: "center", justifyContent: "center" }}>
            <RoundBtn icon="md-wine" text={3} />
            <DotLine />
            <RoundBtn icon="md-wine" text={4} />
          </View> */}
        </View>
        <WorkingAlert working={this.state.working} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    marginTop: 40,
    backgroundColor: "#fff",
  },
});
