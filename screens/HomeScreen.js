import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ToastAndroid,
} from "react-native";
import { WebBrowser } from "expo";
import { Icon } from "expo";
import COLORS from "../constants/Colors";
import WorkingAlert from "../components/WorkingAlert";

const shadowStyles = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,

  elevation: 5,
};

const connect = (pin, status) => {
  fetch(`http://192.168.1.1/pin${pin}${status}`);
};

const RoundBtn = ({ icon, ...rest }) => (
  <TouchableOpacity
    activeOpacity={0.8}
    {...rest}
    style={{
      backgroundColor: COLORS.primary + "90",
      borderRadius: 60,
      borderWidth: 20,
      borderColor: "rgba(3, 169, 244, 0.3)",
      width: 110,
      height: 110,
      alignItems: "center",
      justifyContent: "center",
    }}
  >
    <Icon.Ionicons name={icon} size={26} color="#fff" />
  </TouchableOpacity>
);

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  interval = null;
  state = { working: false };

  timeout = null;

  onLong = async (pin, status) => {
    clearTimeout(this.timeout);
    this.setState({ working: true });
    await connect(
      pin,
      status
    );
  };
  onPressOut = (pin, status) => {
    this.timeout = setTimeout(async () => {
      this.setState({ working: false });
      await connect(
        pin,
        status
      );
    }, 200);
  };
  onPress = () => {
    ToastAndroid.showWithGravity(
      "Przytrzymaj przycisk by wykonać akcję",
      ToastAndroid.SHORT,
      ToastAndroid.CENTER
    );
  };

  render() {
    const { working } = this.state;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.getStartedContainer}>
            <TouchableOpacity
              onPressIn={() => this.onLong(16, "off")}
              onPressOut={() => this.onPressOut(16, "on")}
              style={styles.allUpBtn}
            >
              <Icon.Ionicons name="md-arrow-up" size={26} color="#fff" />
              <Icon.Ionicons name="md-arrow-up" size={26} color="#fff" />
              <Icon.Ionicons name="md-arrow-up" size={26} color="#fff" />
            </TouchableOpacity>
            <View style={styles.partialsBtns}>
              <RoundBtn
                onPressIn={() => this.onLong(15, "off")}
                onPressOut={() => this.onPressOut(15, "on")}
                icon="md-arrow-round-up"
              />
              <RoundBtn
                onPressIn={() => this.onLong(4, "off")}
                onPressOut={() => this.onPressOut(4, "on")}
                icon="md-arrow-round-up"
              />
              <RoundBtn
                onPressIn={() => this.onLong(5, "off")}
                onPressOut={() => this.onPressOut(5, "on")}
                icon="md-arrow-round-up"
              />
            </View>
            <View style={styles.imageWrapper}>
              <Image
                source={require("../assets/images/atlas_bed.png")}
                style={{
                  width: "100%",
                  height: 200,
                }}
                resizeMode="contain"
              />
            </View>
            <View style={styles.partialsBtns}>
              <RoundBtn
                onPressIn={() => this.onLong(3, "off")}
                onPressOut={() => this.onPressOut(3, "on")}
                icon="md-arrow-round-down"
              />
              <RoundBtn
                onPressIn={() => this.onLong(8, "off")}
                onPressOut={() => this.onPressOut(8, "on")}
                icon="md-arrow-round-down"
              />
              <RoundBtn
                onPressIn={() => this.onLong(9, "off")}
                onPressOut={() => this.onPressOut(9, "on")}
                icon="md-arrow-round-down"
              />
            </View>
            <TouchableOpacity
              onPressIn={() => this.onLong(17, "off")}
              onPressOut={() => this.onPressOut(17, "on")}
              style={styles.allDownBtn}
            >
              <Icon.Ionicons name="md-arrow-down" size={26} color="#fff" />
              <Icon.Ionicons name="md-arrow-down" size={26} color="#fff" />
              <Icon.Ionicons name="md-arrow-down" size={26} color="#fff" />
            </TouchableOpacity>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "space-between",
                alignContent: "space-between",
              }}
            >
              <TouchableOpacity
                onPressIn={() => this.onLong(26, "off")}
                onPressOut={() => this.onPressOut(26, "on")}
                style={styles.chairBtn}
              >
                <Text style={{ color: "#fff" }}>Krzesło</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPressIn={() => this.onLong(25, "off")}
                onPressOut={() => this.onPressOut(25, "on")}
                style={styles.chairBtn}
              >
                <Text style={{ color: "#fff" }}>Prostowanie</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    backgroundColor: COLORS.secondary,
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center",
  },
  contentContainer: {
    paddingTop: 30,
    flex: 1,
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20,
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 10,
    justifyContent: "space-around",
    marginVertical: 0,
    flex: 1,
  },
  allUpBtn: {
    width: "100%",
    height: 80,
    backgroundColor: COLORS.primary + "90",
    justifyContent: "space-around",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 40,
    marginBottom: 20,
  },
  allDownBtn: {
    width: "100%",
    height: 80,
    backgroundColor: COLORS.primary + "90",
    justifyContent: "space-around",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 40,
    marginTop: 20,
  },
  partialsBtns: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginHorizontal: 10,
  },
  chairBtn: {
    width: "48%",
    height: 80,
    marginRight: "2%",
    backgroundColor: COLORS.primary + "90",
    justifyContent: "space-around",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 40,
    marginBottom: 20,
  },
  imageWrapper: {
    position: "absolute",
    width: "100%",
    bottom: 100,
    top: 0,
    alignItems: "center",
    justifyContent: "center",
    zIndex: -1,
  },
});
