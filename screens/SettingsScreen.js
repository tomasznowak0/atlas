import React from "react";
import { TouchableOpacity, ScrollView, View, Text, AsyncStorage } from "react-native";

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: "Ustawienia",
  };

  state = {
    primaryColor: null || "#F85931",
  };

  async componentDidMount() {
    AsyncStorage.setItem("primaryColor", this.state.primaryColor);
    let primaryColor = await AsyncStorage.getItem("primaryColor");
    this.setState({ primaryColor });
  }

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
      <ScrollView>
        <TouchableOpacity>
          <Text>first</Text>
          <View style={{ height: 20, width: 20, backgroundColor: this.state.primaryColor }} />
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
