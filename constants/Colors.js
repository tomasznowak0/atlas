const tintColor = "rgba(3, 169, 244, 0.8)";

export default {
  tintColor,
  primary: tintColor,
  secondary: "#fff",
  tabIconDefault: "#ccc",
  tabIconSelected: tintColor,
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff",
};
